<?php

namespace BetaMFD\IssueTrackerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class BetaMFDIssueTrackerBundle extends Bundle
{
}
