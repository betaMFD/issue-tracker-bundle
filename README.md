WIP

This will eventually be something.

You are expected to already have a working database and authentication with a User entity.


Install
```
# composer.json
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/betaMFD/issue-tracker-bundle"
        }
    ],
```

```
composer require betamfd/issue-tracker-bundle:dev-master
```


Make necessary files.

The Issue entity is the meat of this whole project. It's provided as a model so you can add custom fields.
```
// src/Entity/IssueTracker/Issue.php
<?php

namespace App\Entity\IssueTracker;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *   "issue" = "\App\Entity\IssueTracker\Issue",
 *   "task" = "\App\Entity\IssueTracker\Task"
 *   })
 *
 * @ORM\Table(name="issue_tracker_issue")
 * @ORM\Entity()
 */
class Issue extends \BetaMFD\IssueTrackerBundle\Model\Issue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
}
```

Issues are set up as a superclass so that you can make other issue types.
A special type that you need to make is a "Task." The difference between a
Task and an Issue is that Tasks can be set up as recurring while Issues cannot.

Depending on your system you can make up multiple types of special Issues, such as
CARs (Corrective Action Reports) and NCRs (Nonconformity Reports).
I even once set up a special Implementation Plan Issue which was basically
a sub-issue of CARs only (one CAR hand multiple IPs, but no other Issue types did).

Making a Task may be required? Maybe not. Not sure yet.
I suppose it isn't required *yet*.
```
// src/Entity/IssueTracker/Task.php
<?php

namespace App\Entity\IssueTracker;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Task extends Issue
{
}
```


# Update Mapping
# Migration or database update
# Fill statuses, milestones, components


Interface Mapping

```
# app/config/config.yml OR config/packages/doctrine.yaml

doctrine:
    orm:
        resolve_target_entities:
            # Update your App\Entity names as appropriate
            BetaMFD\IssueTrackerBundle\Model\UserInterface: App\Entity\User
            BetaMFD\IssueTrackerBundle\Model\IssueInterface: App\Entity\IssueTracker\Issue

        # IF YOU USE MULTIPLE ENTITY MANAGERS ONLY
        # You will need to add the bundle to your mapping
        entity_managers:
            default:
                connection: default
                naming_strategy: doctrine.orm.naming_strategy.underscore_number_aware
                mappings:
                    App: ~
                    BetaMFDIssueTrackerBundle: ~
```


Note you MUST have a User entity file and it will need to implement the user interface:
```
// src/Entity/User.php
class User implements \BetaMFD\IssueTrackerBundle\Model\UserInterface
{
  // . . .
}
```

The functions required by the interface are:
```
    public function __toString();
    public function getId();
    public function getName(): string;
```
You may need to create those functions.

If you do not have a User entity, you should make one.
A User entity model is intentionally not provided by this bundle.




When you're ready, update your database.
I personally use Doctrine Migrations to do so. But you do you.

Note you will see more tables created than just the ones you made entities for.
This bundle includes as many entities as I felt I could get away with
so that you don't have to build 50-million entities from models.
This isn't a Legos; the building isn't the point.


Although you can get away with making your own statuses, some code may rely on
certain predefined statuses such as "new." Load some initial statuses from
`Resources/sql/status.sql`



Routing!

Depending on what version of symfony you're using this may go in either routing.yaml, or routes.yaml
```
issues:
    resource: "@BetaMFDIssueTrackerBundle/Controller/"
    type:     annotation
    prefix:   /issue_tracker/
    defaults:
      section: ~
```
Prefix can be set to whatever you want
