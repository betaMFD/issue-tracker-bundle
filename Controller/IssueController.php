<?php

namespace BetaMFD\IssueTrackerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IssueController extends AbstractController
{
    /**
     * @Route("/issue/{id}", name="issue")
     */
    public function issueAction(\BetaMFD\IssueTrackerBundle\Model\IssueInterface $issue)
    {
        $return['issue'] = $issue;
        return $this->render('@BetaMFDIssueTracker/issue.html.twig', $return);
    }
}
