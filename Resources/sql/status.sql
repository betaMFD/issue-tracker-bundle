-- starting statuses!
-- you want the first three no matter what, and probably the fourth
INSERT INTO `issue_tracker_statuses` (`id`, `name`, `description`, `is_open`, `default_display`, `sort_order`) VALUES ('new', 'New', 'New Issue; hasn\'t been started.', '1', '1', NULL);
INSERT INTO `issue_tracker_statuses` (`id`, `name`, `description`, `is_open`, `default_display`, `sort_order`) VALUES ('started', 'Started', 'Work has started on this issue and it is in progress.', '1', '1', NULL);
INSERT INTO `issue_tracker_statuses` (`id`, `name`, `description`, `is_open`, `default_display`, `sort_order`) VALUES ('resolved', 'Resolved', 'This issue is resolved.', '0', '0', NULL);

-- put issues on hold for a period of time
INSERT INTO `issue_tracker_statuses` (`id`, `name`, `description`, `is_open`, `default_display`, `sort_order`) VALUES ('hold', 'On Hold', 'This issue is on hold for now.', '1', '0', NULL);

-- the following are optional but you should have at least "closed" if you don't want to mark them as duplicate/invalid/won't fix
INSERT INTO `issue_tracker_statuses` (`id`, `name`, `description`, `is_open`, `default_display`, `sort_order`) VALUES ('duplicate', 'Duplicate', 'This is a duplicate of another issue.', '0', '0', NULL);
INSERT INTO `issue_tracker_statuses` (`id`, `name`, `description`, `is_open`, `default_display`, `sort_order`) VALUES ('invalid', 'Invalid', 'This isn\'t really an issue at all.', '0', '0', NULL);
INSERT INTO `issue_tracker_statuses` (`id`, `name`, `description`, `is_open`, `default_display`, `sort_order`) VALUES ('wontfix', 'Won\'t Fix', 'This is an issue, but we have decided we are not going to do anything about it.', '0', '0', NULL);
INSERT INTO `issue_tracker_statuses` (`id`, `name`, `description`, `is_open`, `default_display`, `sort_order`) VALUES ('closed', 'Closed', 'Catch-all when none of the other statuses seem to work.', '0', '0', NULL);
