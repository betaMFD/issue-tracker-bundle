<?php

namespace BetaMFD\IssueTrackerBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="issue_tracker_issue")
 */
#[ORM\Table(name: "issue_tracker_issue")]
class Issue
{
    // Class that extends this one will need to define this
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    #[ORM\Column(type: "string", length: 150, nullable: false)]
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    #[ORM\Column(type: "text", nullable: true)]
    protected $description;

    /**
     * @var \BetaMFD\IssueTrackerBundle\Model\UserInterface
     * @ORM\ManyToOne(targetEntity="\BetaMFD\IssueTrackerBundle\Model\UserInterface")
     */
    #[ORM\ManyToOne(targetEntity: "\BetaMFD\IssueTrackerBundle\Model\UserInterface")]
    protected $responsible;

    /**
     * @ORM\ManyToMany(targetEntity="\BetaMFD\IssueTrackerBundle\Model\UserInterface")
     * @ORM\JoinTable(name="issue_tracker_issue_participants")
     */
    #[ORM\ManyToMany(targetEntity: "\BetaMFD\IssueTrackerBundle\Model\UserInterface")]
    #[ORM\JoinTable(name: "issue_tracker_issue_participants")]
    protected $participants;

    /**
     * @ORM\ManyToMany(targetEntity="\BetaMFD\IssueTrackerBundle\Model\UserInterface", inversedBy="issuesWatching")
     * @ORM\JoinTable(name="issue_tracker_issue_watchers")
     */
    #[ORM\ManyToMany(targetEntity: "\BetaMFD\IssueTrackerBundle\Model\UserInterface", inversedBy: "issuesWatching")]
    #[ORM\JoinTable(name: "issue_tracker_issue_watchers")]
    protected $watchers;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    #[ORM\Column(type: "date", nullable: true)]
    protected $deadline;

    /**
     * @ORM\ManyToMany(targetEntity="\BetaMFD\IssueTrackerBundle\Model\UserInterface")
     * @ORM\JoinTable(name="issue_tracker_issue_votes")
     */
    #[ORM\ManyToMany(targetEntity: "\BetaMFD\IssueTrackerBundle\Model\UserInterface")]
    #[ORM\JoinTable(name: "issue_tracker_issue_votes")]
    protected $votes;

    /**
     * @var \BetaMFD\IssueTrackerBundle\Entity\Status
     * @ORM\ManyToOne(targetEntity="\BetaMFD\IssueTrackerBundle\Entity\Status")
     * @ORM\JoinColumn(nullable=false)
     */
    #[ORM\ManyToOne(targetEntity: "\BetaMFD\IssueTrackerBundle\Entity\Status")]
    #[ORM\JoinColumn(nullable: false)]
    protected $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    protected $holdExpireDate;

    /**
     * @var \BetaMFD\IssueTrackerBundle\Entity\Status
     * @ORM\ManyToOne(targetEntity="\BetaMFD\IssueTrackerBundle\Entity\Status")
     */
    #[ORM\ManyToOne(targetEntity: "\BetaMFD\IssueTrackerBundle\Entity\Status")]
    protected $holdExpireStatus;

    /**
     * @var \BetaMFD\IssueTrackerBundle\Entity\Component
     * @ORM\ManyToOne(targetEntity="\BetaMFD\IssueTrackerBundle\Entity\Component")
     */
    #[ORM\ManyToOne(targetEntity: "\BetaMFD\IssueTrackerBundle\Entity\Component")]
    protected $component;

    /**
     * @var \BetaMFD\IssueTrackerBundle\Entity\Milestone
     * @ORM\ManyToOne(targetEntity="\BetaMFD\IssueTrackerBundle\Entity\Milestone")
     */
    #[ORM\ManyToOne(targetEntity: "\BetaMFD\IssueTrackerBundle\Entity\Milestone")]
    protected $milestone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    #[ORM\Column(type: "datetime", nullable: false)]
    protected $createdDate;

    /**
     * @var \BetaMFD\IssueTrackerBundle\Model\UserInterface
     * @ORM\ManyToOne(targetEntity="\BetaMFD\IssueTrackerBundle\Model\UserInterface")
     */
    #[ORM\ManyToOne(targetEntity: "\BetaMFD\IssueTrackerBundle\Model\UserInterface")]
    protected $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    protected $updatedDate;

    /**
     * @var \BetaMFD\IssueTrackerBundle\Model\UserInterface
     * @ORM\ManyToOne(targetEntity="\BetaMFD\IssueTrackerBundle\Model\UserInterface")
     */
    #[ORM\ManyToOne(targetEntity: "\BetaMFD\IssueTrackerBundle\Model\UserInterface")]
    protected $updatedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    #[ORM\Column(type: "datetime", nullable: true)]
    protected $closedDate;

    /**
     * @var \BetaMFD\IssueTrackerBundle\Model\UserInterface
     * @ORM\ManyToOne(targetEntity="\BetaMFD\IssueTrackerBundle\Model\UserInterface")
     */
    #[ORM\ManyToOne(targetEntity: "\BetaMFD\IssueTrackerBundle\Model\UserInterface")]
    protected $closedBy;

    /**
     * Temporary information used when adding new participants
     * Helps with logging
     * @var \BetaMFD\IssueTrackerBundle\Model\UserInterface
     */
    protected $newParticipant;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="\BetaMFD\IssueTrackerBundle\Entity\IssueChecklist",
     *  mappedBy="issue",
     *  cascade={"persist", "remove"})
     * @ORM\OrderBy({"orderNumber" = "ASC"})
     */
    #[ORM\OneToMany(targetEntity: "\BetaMFD\IssueTrackerBundle\Entity\IssueChecklist", mappedBy: "issue", cascade: ["persist", "remove"])]
    #[ORM\OrderBy(["orderNumber" => "ASC"])]
    protected $checklistItems;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="\BetaMFD\IssueTrackerBundle\Model\IssueInterface", mappedBy="superIssue")
     */
    #[ORM\OneToMany(targetEntity: "\BetaMFD\IssueTrackerBundle\Model\IssueInterface", mappedBy: "superIssue")]
    protected $subIssues;

    /**
     * @var \BetaMFD\IssueTrackerBundle\Model\IssueInterface
     *
     * @ORM\ManytoOne(targetEntity="\BetaMFD\IssueTrackerBundle\Model\IssueInterface", inversedBy="subIssues")
     */
    #[ORM\ManyToOne(targetEntity: "\BetaMFD\IssueTrackerBundle\Model\IssueInterface", inversedBy: "subIssues")]
    protected $superIssue;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    #[ORM\Column(type: "boolean", nullable: false)]
    protected $showOnIssueTracker = true;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    #[ORM\Column(type: "boolean", nullable: false)]
    protected $restricted = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    #[ORM\Column(type: "boolean", nullable: false)]
    protected $personal = false;


    public function __construct(?\BetaMFD\IssueTrackerBundle\Entity\Status $status)
    {
        $this->createdDate = new \DateTime();
        $this->status = $status;
        $this->votes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->participants = new \Doctrine\Common\Collections\ArrayCollection();
        $this->watchers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return '#' . $this->id . ' ' . $this->title;
    }

    public function new(\BetaMFD\IssueTrackerBundle\Model\UserInterface $user)
    {
        $this->setCreatedBy($user);
    }

    public function isIssue() {
        return true;
    }

    public function isStatus($status)
    {
        return $this->status->getId() == $status or $this->status->getName() == $status;
    }

    public function isOpen()
    {
        return $this->status->isOpen();
    }

    public function isResolvable()
    {
        return true;
    }

    public function isMember(\BetaMFD\IssueTrackerBundle\Model\UserInterface $user)
    {
        return $this->isCreator($user)
            or $this->isResponsible($user)
            or $this->isParticipant($user);
    }

    public function canSee($user) {
        if (!$this->isPrivate()) {
            //issue isn't private, everything's cool
            return true;
        }
        return $this->isMember($user);
    }

    /**
     * Get the value of Id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of Title
     *
     * @param string $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        // limit the title to 150 characters in case something or someone gets wordy
        $this->title = substr($title, 0, 150);

        return $this;
    }

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description
     *
     * @param string $description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Responsible
     *
     * @return \BetaMFD\IssueTrackerBundle\Model\UserInterface
     */
    public function getResponsible()
    {
        return $this->responsible;
    }

    public function isResponsible(\BetaMFD\IssueTrackerBundle\Model\UserInterface $user)
    {
        return $this->responsible == $user;
    }

    /**
     * Set the value of Responsible
     *
     * @param \BetaMFD\IssueTrackerBundle\Model\UserInterface $responsible
     *
     * @return self
     */
    public function setResponsible(\BetaMFD\IssueTrackerBundle\Model\UserInterface $responsible = null)
    {
        $this->responsible = $responsible;
        if (!empty($responsible)) {
            $this->addWatcher($responsible);
        }

        return $this;
    }

    /**
     * Get the value of Participants
     *
     * @return mixed
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    public function isParticipant(\BetaMFD\IssueTrackerBundle\Model\UserInterface $user)
    {
        return $this->participants->contains($user);
    }

    /**
     * Get the value of Participants
     *
     * @return mixed
     */
    public function getParticipantsIdArray()
    {
        $new = [];
        foreach ($this->participants as $p) {
            $new[] = $p->getId();
        }
        return $new;
    }

    /**
     * Add participant
     *
     * @param \BetaMFD\IssueTrackerBundle\Model\UserInterface $user
     *
     * @return self
     */
    public function addParticipant(\BetaMFD\IssueTrackerBundle\Model\UserInterface $user)
    {
        if (
            $this->responsible !== $user
            and !$this->participants->contains($user)
        ) {
            $this->participants[] = $user;
            $this->addWatcher($user);
        }
    }

    /**
     * Remove participant
     *
     * @param \BetaMFD\IssueTrackerBundle\Model\UserInterface $user
     */
    public function removeParticipant(\BetaMFD\IssueTrackerBundle\Model\UserInterface $user)
    {
        $this->participants->removeElement($user);
        //if participant is a watcher, remove them
        if ($this->isPrivate() && $this->watchers->contains($user)) {
            $this->watchers->removeElement($user);
        }
    }

    /**
     * Get the value of Watchers
     *
     * @return mixed
     */
    public function getWatchers()
    {
        return $this->watchers;
    }

    public function isWatcher(\BetaMFD\IssueTrackerBundle\Model\UserInterface $user)
    {
        return $this->watchers->contains($user);
    }

    /**
     * Add watcher
     *
     * @param \BetaMFD\IssueTrackerBundle\Model\UserInterface $user
     *
     * @return self
     */
    public function addWatcher(\BetaMFD\IssueTrackerBundle\Model\UserInterface $user)
    {
        if (!$this->watchers->contains($user)) {
            $this->watchers[] = $user;
        }
    }

    /**
     * Remove watcher
     *
     * @param \BetaMFD\IssueTrackerBundle\Model\UserInterface $user
     */
    public function removeWatcher(\BetaMFD\IssueTrackerBundle\Model\UserInterface $user)
    {
        $this->watchers->removeElement($user);
    }

    /**
     * Add or remove watcher
     *
     * @param \BetaMFD\IssueTrackerBundle\Model\UserInterface $user
     */
    public function toggleWatcher(\BetaMFD\IssueTrackerBundle\Model\UserInterface $user)
    {
        if (!$this->watchers->contains($user)) {
            $this->watchers[] = $user;
        } else {
            $this->watchers->removeElement($user);
        }

        return $this;
    }

    public function getWatcherCount()
    {
        return $this->watchers->count();
    }

    /**
     * Get the value of Deadline
     *
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * Set the value of Deadline
     *
     * @param \DateTime $deadline
     *
     * @return self
     */
    public function setDeadline(?\DateTime $deadline)
    {
        $this->deadline = $deadline;

        return $this;
    }

    /**
     * Get the value of Votes
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getVotes()
    {
        return $this->votes;
    }

    /**
     * Get the value of Status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the value of Status
     *
     * @return string
     */
    public function getStatusCSS()
    {
        //remove special characters
        $s = str_replace(['\''], '', $this->status);
        //replace spaces with -
        $s = str_replace(' ', '-', $s);
        return $s;
    }

    //for the moment need to leave out setStatus. Will probably fix later

    /**
     * Get the value of Hold Expire Date
     *
     * @return \DateTime
     */
    public function getHoldExpireDate()
    {
        return $this->holdExpireDate;
    }

    /**
     * Set the value of Hold Expire Date
     *
     * @param \DateTime $holdExpireDate
     *
     * @return self
     */
    public function setHoldExpireDate(\DateTime $holdExpireDate)
    {
        $this->holdExpireDate = $holdExpireDate;

        return $this;
    }

    /**
     * Get the value of Hold Expire Status
     *
     * @return \BetaMFD\IssueTrackerBundle\Entity\Status
     */
    public function getHoldExpireStatus()
    {
        return $this->holdExpireStatus;
    }

    /**
     * Set the value of Hold Expire Status
     *
     * @param \BetaMFD\IssueTrackerBundle\Entity\Status $holdExpireStatus
     *
     * @return self
     */
    public function setHoldExpireStatus(\BetaMFD\IssueTrackerBundle\Entity\Status $holdExpireStatus = null)
    {
        $this->holdExpireStatus = $holdExpireStatus;

        return $this;
    }

    /**
     * Get the value of Created Date
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Get the value of Component
     *
     * @return \BetaMFD\IssueTrackerBundle\Entity\Component
     */
    public function getComponent()
    {
        return $this->component;
    }

    /**
     * Set the value of Component
     *
     * @param \BetaMFD\IssueTrackerBundle\Entity\Component component
     *
     * @return self
     */
    public function setComponent(\BetaMFD\IssueTrackerBundle\Entity\Component $component = null)
    {
        $this->component = $component;

        return $this;
    }

    /**
     * Get the value of Milestone
     *
     * @return \BetaMFD\IssueTrackerBundle\Entity\Milestone
     */
    public function getMilestone()
    {
        return $this->milestone;
    }

    /**
     * Set the value of Milestone
     *
     * @param \BetaMFD\IssueTrackerBundle\Entity\Milestone milestone
     *
     * @return self
     */
    public function setMilestone(\BetaMFD\IssueTrackerBundle\Entity\Milestone $milestone = null)
    {
        $this->milestone = $milestone;

        return $this;
    }

    /**
     * Set the value of Created Date
     *
     * @param \DateTime $createdDate
     *
     * @return self
     */
    public function setCreatedDate(\DateTime $createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get the value of Created By
     *
     * @return \BetaMFD\IssueTrackerBundle\Model\UserInterface
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function isCreator(\BetaMFD\IssueTrackerBundle\Model\UserInterface $user)
    {
        return $this->createdBy == $user;
    }

    /**
     * Set the value of Created By
     *
     * @param \BetaMFD\IssueTrackerBundle\Model\UserInterface $createdBy
     *
     * @return self
     */
    public function setCreatedBy(\BetaMFD\IssueTrackerBundle\Model\UserInterface $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get the value of Updated Date
     *
     * @return \DateTime
     */
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * Set the value of Updated Date
     *
     * @param \DateTime $updatedDate
     *
     * @return self
     */
    public function setUpdatedDate(\DateTime $updatedDate)
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * Get the value of Updated By
     *
     * @return \BetaMFD\IssueTrackerBundle\Model\UserInterface
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set the value of Updated By
     *
     * @param \BetaMFD\IssueTrackerBundle\Model\UserInterface $updatedBy
     *
     * @return self
     */
    public function setUpdatedBy(\BetaMFD\IssueTrackerBundle\Model\UserInterface $updatedBy)
    {
        $this->updatedBy = $updatedBy;
        $this->setUpdatedDate(new \DateTime);

        return $this;
    }

    /**
     * Get the value of Closed Date
     *
     * @return \DateTime
     */
    public function getClosedDate()
    {
        return $this->closedDate;
    }

    /**
     * Set the value of Closed Date
     *
     * @param \DateTime $closedDate
     *
     * @return self
     */
    public function setClosedDate(?\DateTime $closedDate)
    {
        $this->closedDate = $closedDate;

        return $this;
    }

    /**
     * Get the value of Closed By
     *
     * @return \BetaMFD\IssueTrackerBundle\Model\UserInterface
     */
    public function getClosedBy()
    {
        return $this->closedBy;
    }

    /**
     * Set the value of Closed By
     *
     * @param \BetaMFD\IssueTrackerBundle\Model\UserInterface $closedBy
     *
     * @return self
     */
    public function setClosedBy(?\BetaMFD\IssueTrackerBundle\Model\UserInterface $closedBy)
    {
        $this->closedBy = $closedBy;
        if (!empty($closedBy)) {
            $this->setClosedDate(new \DateTime);
        } else {
            $this->setClosedDate(null);
        }

        return $this;
    }

    /**
     * Get the value of Temporary information used when adding new participants
     *
     * @return \BetaMFD\IssueTrackerBundle\Model\UserInterface
     */
    public function getNewParticipant()
    {
        return $this->newParticipant;
    }

    /**
     * Set the value of Temporary information used when adding new participants
     *
     * @param \BetaMFD\IssueTrackerBundle\Model\UserInterface $newParticipant
     *
     * @return self
     */
    public function setNewParticipant(\BetaMFD\IssueTrackerBundle\Model\UserInterface $newParticipant)
    {
        $this->newParticipant = $newParticipant;

        return $this;
    }

    /**
     * Get the value of Checklist Items
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getChecklistItems()
    {
        return $this->checklistItems;
    }

    public function hasChecklist()
    {
        return !$this->checklistItems->isEmpty();
    }

    public function newChecklistItem($text)
    {
        $item = new \BetaMFD\IssueTrackerBundle\Entity\IssueChecklist($text, $this);
        $this->checklistItems[] = $item;

        return $this;
    }

    /**
     * Get the value of Sub Issues
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSubIssues()
    {
        return $this->subIssues;
    }

    public function hasSubIssues()
    {
        return !$this->subIssues->isEmpty();
    }

    public function addSubIssue(\BetaMFD\IssueTrackerBundle\Model\IssueInterface $issue)
    {
        $this->subIssues[] = $issue;
    }

    public function removeSubIssue(
        \BetaMFD\IssueTrackerBundle\Model\IssueInterface $issue,
        \BetaMFD\IssueTrackerBundle\Model\UserInterface $user
    ) {
        if ($this->subIssues->contains($issue)) {
            $this->subIssues->removeElement($issue);
            $this->logSubRemove($user, $issue);
            $issue->setSuperIssue(null);
            $issue->logSuperChange($user, $this);
        }
    }

    /**
     * Get the value of Super Issue
     *
     * @return \BetaMFD\IssueTrackerBundle\Model\IssueInterface
     */
    public function getSuperIssue()
    {
        return $this->superIssue;
    }

    /**
     * Set the value of Super Issue
     *
     * @param \BetaMFD\IssueTrackerBundle\Model\IssueInterface $superIssue
     *
     * @return self
     */
    public function setSuperIssue(\BetaMFD\IssueTrackerBundle\Model\IssueInterface $superIssue = null)
    {
        $this->superIssue = $superIssue;

        return $this;
    }

    /**
     * Get the value of Show On Issue Tracker
     *
     * @return boolean
     */
    public function getShowOnIssueTracker()
    {
        return $this->showOnIssueTracker;
    }

    /**
     * Set the value of Show On Issue Tracker
     *
     * @param boolean $showOnIssueTracker
     *
     * @return self
     */
    public function setShowOnIssueTracker($showOnIssueTracker)
    {
        $this->showOnIssueTracker = $showOnIssueTracker;

        return $this;
    }

    /**
     * Get the value of Restricted
     *
     * @return boolean
     */
    public function getRestricted()
    {
        return $this->restricted;
    }

    /**
     * Set the value of Restricted
     *
     * @param boolean $restricted
     *
     * @return self
     */
    public function setRestricted($restricted)
    {
        $this->restricted = $restricted;

        return $this;
    }

    /**
     * Get the value of Personal
     *
     * @return boolean
     */
    public function getPersonal()
    {
        return $this->personal;
    }

    /**
     * Set the value of Personal
     *
     * @param boolean $personal
     *
     * @return self
     */
    public function setPersonal($personal)
    {
        $this->personal = $personal;

        return $this;
    }

    /**
     * If restricted or personal
     * @return boolean
     */
    public function isPrivate()
    {
        return $this->getPersonal() or $this->getRestricted();
    }
}
