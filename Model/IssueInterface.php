<?php

namespace BetaMFD\IssueTrackerBundle\Model;

interface IssueInterface
{
    public function __toString();
    public function getTitle();
    public function isIssue();
    public function isStatus($status);
}
