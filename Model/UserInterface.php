<?php

namespace BetaMFD\IssueTrackerBundle\Model;

interface UserInterface
{
    public function __toString(): string;
    public function getId();
    public function getName(): string;
}
