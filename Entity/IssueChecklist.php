<?php

namespace BetaMFD\IssueTrackerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="issue_tracker_issue_checklist")
 * @ORM\Entity()
 */
#[ORM\Table(name: "issue_tracker_issue_checklist")]
#[ORM\Entity]
class IssueChecklist
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    #[ORM\Column(name: "id", type: "integer")]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "IDENTITY")]
    protected $id;

    /**
     * @var \BetaMFD\IssueTrackerBundle\Model\IssueInterface
     * @ORM\ManyToOne(targetEntity="\BetaMFD\IssueTrackerBundle\Model\IssueInterface", inversedBy="checklistItems")
     */
    #[ORM\ManyToOne(targetEntity: "\BetaMFD\IssueTrackerBundle\Model\IssueInterface", inversedBy: "checklistItems")]
    protected $issue;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    #[ORM\Column(type: "string", length: 150, nullable: false)]
    protected $title;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    #[ORM\Column(type: "boolean", nullable: false)]
    protected $checked = false;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    #[ORM\Column(type: "integer")]
    protected $orderNumber = 100;

    public function __construct(
        $title,
        \BetaMFD\IssueTrackerBundle\Model\IssueInterface $issue
    ) {
        $this->title = $title;
        $this->issue = $issue;
    }

    /**
     * Get the value of Id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param integer $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Issue
     *
     * @return \BetaMFD\IssueTrackerBundle\Model\IssueInterface
     */
    public function getIssue()
    {
        return $this->issue;
    }

    /**
     * Set the value of Issue
     *
     * @param \BetaMFD\IssueTrackerBundle\Model\IssueInterface $issue
     *
     * @return self
     */
    public function setIssue(\BetaMFD\IssueTrackerBundle\Model\IssueInterface $issue)
    {
        $this->issue = $issue;

        return $this;
    }

    /**
     * Get the value of Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of Title
     *
     * @param string $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of Checked
     *
     * @return boolean
     */
    public function getChecked()
    {
        return $this->checked;
    }

    public function toggleChecked()
    {
        if ($this->checked) {
            $this->checked = false;
        } else {
            $this->checked = true;
        }

        return $this;
    }

    /**
     * Set the value of Checked
     *
     * @param boolean $checked
     *
     * @return self
     */
    public function setChecked($checked)
    {
        $this->checked = $checked;

        return $this;
    }

    /**
     * Get the value of Order Number
     *
     * @return integer
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * Set the value of Order Number
     *
     * @param integer $orderNumber
     *
     * @return self
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }
}
