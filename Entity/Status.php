<?php

namespace BetaMFD\IssueTrackerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="issue_tracker_statuses")
 * @ORM\Entity()
 */
#[ORM\Table(name: "issue_tracker_statuses")]
#[ORM\Entity]
class Status
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(type="string", length=50)
     */
    #[ORM\Id]
    #[ORM\Column(type: "string", length: 50)]
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    #[ORM\Column(type: "string", length: 50, nullable: false)]
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false)
     */
    #[ORM\Column(type: "text", nullable: false)]
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    #[ORM\Column(type: "boolean", nullable: false)]
    private $isOpen;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    #[ORM\Column(type: "boolean", nullable: false)]
    private $defaultDisplay;

    /**
     * @var integer
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    #[ORM\Column(name: "sort_order", type: "integer", nullable: true)]
    private $order;

    public function __toString()
    {
        return $this->id;
    }

    public function isOpen()
    {
        return $this->isOpen;
    }

    /**
     * Get the value of Id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param string $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of Description
     *
     * @param string $description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the value of Is Open
     *
     * @return boolean
     */
    public function getIsOpen()
    {
        return $this->isOpen;
    }

    /**
     * Set the value of Is Open
     *
     * @param boolean $isOpen
     *
     * @return self
     */
    public function setIsOpen($isOpen)
    {
        $this->isOpen = $isOpen;

        return $this;
    }

    /**
     * Get the value of Default Display
     *
     * @return boolean
     */
    public function getDefaultDisplay()
    {
        return $this->defaultDisplay;
    }

    /**
     * Set the value of Default Display
     *
     * @param boolean $defaultDisplay
     *
     * @return self
     */
    public function setDefaultDisplay($defaultDisplay)
    {
        $this->defaultDisplay = $defaultDisplay;

        return $this;
    }

    /**
     * Get the value of Order
     *
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set the value of Order
     *
     * @param integer $order
     *
     * @return self
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }
}
