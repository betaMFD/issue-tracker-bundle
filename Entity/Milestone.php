<?php

namespace BetaMFD\IssueTrackerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="issue_tracker_milestone")
 * @ORM\Entity()
 */
#[ORM\Table(name: "issue_tracker_milestone")]
#[ORM\Entity]
class Milestone
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    #[ORM\Column(name: "id", type: "integer")]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "IDENTITY")]
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    #[ORM\Column(type: "string", length: 150, nullable: false)]
    private $milestone;


    public function __toString()
    {
        return $this->milestone;
    }

    /**
     * Get the value of Id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Milestone
     *
     * @return string
     */
    public function getMilestone()
    {
        return $this->milestone;
    }

    /**
     * Set the value of Milestone
     *
     * @param string milestone
     *
     * @return self
     */
    public function setMilestone($milestone)
    {
        $this->milestone = $milestone;

        return $this;
    }
}
