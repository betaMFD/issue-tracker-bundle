<?php

namespace BetaMFD\IssueTrackerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('beta_mfd_issue_tracker');
        if (method_exists($treeBuilder, 'getRootNode')) {
            $rootNode = $treeBuilder->getRootNode();
        } else {
            $rootNode = $treeBuilder->root('beta_mfd_issue_tracker');
        }

        $rootNode
            ->children()
                ->scalarNode('project_dir')
                ->defaultValue('%kernel.project_dir%')
            ->end()
        ;

        return $treeBuilder;
    }
}
